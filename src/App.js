import React from "react";
import { API_KEY } from "./constants";

const App = () => {

    let API_KEY = "3c022482366ce4938ba96367e62c42aa"

    const [weather, setWeather] = React.useState([]);
    const [location, setLocation] = React.useState([]);
    const [icon, setIcon] = React.useState(false);
    const [description, setDescription] = React.useState(null);

    
    // update state value for input
    const onChangeL = (event) => {
        setLocation(event.target.value);
      };

    // api call
    let weatherRequest = () => {
        fetch(`https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${API_KEY}`)
        .then(response => response.json())
        .then(data => {
            setWeather(data)
            setIcon(data.weather[0].icon)
            setDescription(data.weather[0].description)
        })
      // displayIcons();
    }
    
    // want to use to display icons?
    // let displayIcons = () => {
    //   `http://openweathermap.org/img/wn/{icon}@2x.png`
    // }

  return (
      <div>
        <input value={location} onChange={onChangeL} className="text-input" type="text" placeholder="Search a city" />
        <button onClick={weatherRequest}>Search</button>
        {icon !== false && 
          <img src={`http://openweathermap.org/img/wn/${icon}@2x.png`}></img>
        }
        <div>
        {description !== null && 
          <p>{description}</p>
        }
        </div>
      </div>
  );
};

export default App;
